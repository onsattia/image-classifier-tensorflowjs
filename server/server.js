let express = require("express");
let app = express();

app.use((req, res, next) => {
  console.log(`${new Date()} - ${req.method} reqest for ${req.url}`);
  next();
});

app.use(express.static("../client"));

app.listen(3000, () => {
  console.log("Serving Running at 3000...");
});
